This is a new project template. Just copy the content to a new folder, init a git repo and start developing. Also don't forget about `npm install` or better `yarn`.

You can use `gulp watch` to start the browserSync server.

For image sprites put the files into `images/icons` and run `gulp sprite`.

To get all gulp commands available see `gulpfile.js`