function addClass (element, newClass){

    // Check if elemnt exists, exit if not
    if (!element){
        return;
    } 

    // if the element has no class, just add a new class
    else if (element.getAttribute('class') == null){
        element.setAttribute('class', newClass);
    } 

    // if the class already exists, exit
    else if (hasClass(element, newClass)){
        return;
    } 

    // If the element already has a class, add the new one to the list
    else {
        element.setAttribute('class', element.getAttribute('class') + ' ' + newClass);
    }
}

function removeClass (element, className){
    // Check if element exists, exit if not
    if (!element) {
        return;
    }

    // first get the current class list, get rid of the desired class and return new state
    var newState = element.getAttribute('class').replace(new RegExp('(\\s|^)' + className + '(\\s|$)', 'g'), '$2');

    //set the new class list
    element.setAttribute('class', newState);
}

function hasClass (element, className){

    // test if the element has contains given class
    // return TRUE if yes
    return new RegExp('(\\s|^)' + className + '(\\s|$)').test(element.getAttribute('class'));
}

function getElementIndex(element, parentElement) {
    // determine numerical position (index) of the element within the parrent
    var index = Array.prototype.indexOf.call(parentElement.children, element);
    return index;
}

function toggleClass(element, className) {
    // Determine whether the element contains given class
    var visible = element.classList.contains(className);

    // if yes, remove it
    if (visible) {
        removeClass(element, className);
    } 

    // if not, add it to the list
    else {
        addClass(element, className);
    }
}

// sets display (block by default) to given a given element
// if it is an array or a nodelist, hides all elements but the one with given index
/* Commong usage with element:
    element.addEventListner('click', function(event){
        toggleView(element, 'flex');
    });
/* Common usage with array:
    element.addEventListener('click', function(event){
        index = clickItem(event);
        toggleView(elementItems, 'block', index);
    });
*/
function toggleView(element, display, index){

    display = display || 'block';

    // if element is part of NodeList (querySelectolAll) or of array
    // loop through all elements and hide them first.
    // Then set display style to a single element based on index of clicked item within parrent node
    if(isNodeList(element) || isArray(element)) {
        if(element && index < element.length){
            for (i = 0; i < element.length; i++){
            element[i].style.display = 'none';
            }

            element[index].style.display = display;
        }
    // if element is a single item, get its current style and toggle between displays
    } else if (element){
        if (getStyle(element, 'display') === 'none'){
            element.style.display = display;
        } else {
            element.style.display = 'none';
        }
    // fallback if no element or unexpected format was given
    } else {
        return;
    }
}

// get current property of given element
function getStyle(el, prop) {
    // get the actual displayed style (may differ from css initial setting)
    var getComputedStyle = window.getComputedStyle;

    // currentStyle is for IE
    return ( getComputedStyle ? getComputedStyle(el) : el.currentStyle )[
            // transforms css property into camel sized javascript version
            prop.replace(/-(\w)/gi, function (word, letter) {
                return letter.toUpperCase();   
            })
        ];
}

// check if element belongs to a node list (querySelectorAll for instance)
function isNodeList(element){
    return NodeList.prototype.isPrototypeOf(element)
}


function isArray(element){
    // older browsers don't support isArray method
    if (!Array.isArray) {
        return Object.prototype.toString.call(element) === '[object Array]';
    } else {
        return Array.isArray(element);
    }
}

// debounce multiple events
// if immediate is passed, trigger the function on the leading edge instead of trailer
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

// sets a a maximum number of times a function can be called over time
function throttle (callback, limit) {
    var wait = false;                
    return function () {             
        if (!wait) {                 
            callback.call();          
            wait = true;              
            setTimeout(function () {  
                wait = false;         
            }, limit);
        }
    }
}

// return the maximum height from an array of objects
function getMaxHeight(items){

    var maxHeight = 0;
    var height = 0;
    var i, j;

    for(i = 0; i < items.length; i++){

        height = items[i].clientHeight;

        if(height > maxHeight){
            maxHeight = height;
        }

        height = 0;

    }
    return maxHeight;
}

// first determine the maximum height among the given projects
// then set the same height in pixels to all of them
/* Common ussage: (prevents from executing function too often)
        window.onresize = debounce(function(){
            setSameHeight(tilesArray);
        }, 250);
*/ 
function setSameHeight(tilesArray){

    for (i = 0; i < tilesArray.length; i++){
        tilesArray[i].style.height = "";
    }

    var maxHeight = getMaxHeight(tilesArray);
    
    for (i = 0; i < tilesArray.length; i++){
        tilesArray[i].style.height = maxHeight + "px";
    }
}

function makeRequest(url, callback) {

    var httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        return false;
    }

    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState == 4 && httpRequest.status == 200) {
            callback(httpRequest.responseText);
        }
    };

    httpRequest.open('GET', url);
    httpRequest.send();
}

function parseSafeJSON(data) {
    var jsonString = data.replace(")]}',", "");
    return JSON.parse(jsonString);
}

//safely registers event listener
function addEvent(object, event, callback) {

    if (object == null || typeof(object) == 'undefined') return;

    if (object.addEventListener) {
        object.addEventListener(event, callback, false);

    } else if (object.attachEvent) {
        object.attachEvent("on" + event, callback);

    } else {
        object["on" + event] = callback;
    }
}